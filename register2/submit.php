<?php 

require_once("dbconfig.php");

$firstname = "";
$lastname = "";
$phone = "";
$email = "";
$cnp = "";
$sex = "";
$facebook = "";
$facultate = "";
$birth = "";
$department = "";
$question = "";
$captcha_generated = "";
$captcha_inserted = "";
$check = "";
$error = 0;
$error_text = "";
$i = 0;




if(isset($_POST['firstname'])){
	$firstname = $_POST['firstname'];
}

if(isset($_POST['facultate'])){
	$facultate = $_POST['facultate'];
}

if(isset($_POST['lastname'])){
	$lastname = $_POST['lastname'];
}

if(isset($_POST['phone'])){
	$phone = $_POST['phone'];
}

if(isset($_POST['email'])){
	$email = $_POST['email'];
}

if(isset($_POST['cnp'])){
	$cnp = $_POST['cnp'];
}

if(isset($_POST['facebook'])){
	$facebook = $_POST['facebook'];
}

if(isset($_POST['birth'])){
	$birth = $_POST['birth'];
}

if(isset($_POST['department'])){
	$department = $_POST['department'];
}

if(isset($_POST['question'])){
	$question = $_POST['question'];
}

if(isset($_POST['captcha_generated'])){
	$captcha_generated = $_POST['captcha_generated'];
}

if(isset($_POST['captcha_inserted'])){
	$captcha_inserted = $_POST['captcha_inserted'];
}

if(isset($_POST['check'])){
	$check = $_POST['check'];
}

if ($cnp[0]==1 || $cnp[0]==3 || $cnp[0]==5) $sex='M';
if ($cnp[0]==2 || $cnp[0]==4 || $cnp[0]==6)  $sex='F';

if(empty($firstname) || empty($lastname) || empty($phone) || empty($email) || empty($facebook) || empty($birth) || empty($department) || empty($question) || empty($captcha_generated) ||empty($captcha_inserted) || empty($check) || empty($facultate)){
	$error = 1;
	$error_text = "One or more fields are empty!"; }

if(strlen($firstname) < 3 || strlen($lastname) < 3) {
	$error = 1;
	$error_text = "First or Last name is shorter than expected!"; }

if(strlen($facultate) < 3) {
	$error = 1;
	$error_text = "College name is shorter than expected!"; }

if(strlen($facultate) > 30) {
	$error = 1;
	$error_text = "College name is longer than expected!"; }

if(is_numeric($facultate) ){
	$error = 1;
	$error_text = "College name is not valid"; }

if(strlen($firstname) > 20 ){ 
	$error = 1;
	$error_text = "Firstname is longer than expected!"; }

	if(strlen($lastname) > 20 ) {
		$error = 1;
	$error_text = "Lastname is longer than expected!";}

	if(strlen($question) < 15) {
	$error = 1;
	$error_text = "Question is shorter than expected!";
}
if(is_numeric($firstname) || is_numeric($lastname) ){
	$error = 1;
	$error_text = "First and last name are not valid";

}

if(!is_numeric($phone) || strlen($phone)!=10){
	$error = 1;
	$error_text = "Phone number is not valid";
} 

 
if(!(filter_var($email, FILTER_VALIDATE_EMAIL)) )
     { $error = 1;
	$error_text = "Email is not valid"; }

while ($i<strlen($email) && $email[$i]!='@')
	{if (!($email[$i]>='a' && $email[$i]<='z' || $email[$i]>='0' && $email[$i]<='9' || $email[$i]=='.' || $email[$i]=='-' || $email[$i]=='_' )) 
  { $error = 1;
	$error_text = "Email is not valid"; }
$i++;}

if (strlen($cnp)!=13 || $cnp[0]==0 || $cnp[0]==7 || $cnp[0]==8 || $cnp[0]==9){
	$error = 1;
	$error_text = "CNP is not valid";}


if((stristr($facebook,"www.facebook.com/")!==$facebook) || strstr($facebook, "www.facebook.com/")=="www.facebook.com/")

{
	$error = 1;
	$error_text = "Facebook is not valid";}


else { 
         for ($i=strlen("www.facebook.com/")+1; $i<strlen($facebook); $i++) 
         	{if(!($facebook[$i]>='a' && $facebook[$i]<='z' || $facebook[$i]>='0' && $facebook[$i]<='9' || $facebook[$i]=='.' || $facebook[$i]=='-' || $facebook[$i]=='_' )) 

         		{ $error = 1;
	$error_text = "Facebook is not valid";} }
 }

$age = date_diff(date_create($birth), date_create('now'))->y;
if ($age < 18 ) { $error = 1;
	$error_text = "Too young";} 
if ($age >100 ) { $error = 1;
	$error_text = "Too old";} 

if ($captcha_generated !== $captcha_inserted) { $error = 1;
	$error_text = "Wrong captcha";} 

try {

    $con = new pdo('mysql:host=' . HOST . ';dbname=' . DATABASE . ';charset=utf8;', USER, PASSWORD);
    
} catch(Exception $e) {

    $db_error['connection'] = "Cannot connect to database";

    $response = json_encode($db_error);

    

        echo $response;
    return;

}

$stmt = $con->prepare("SELECT count(*) FROM register2");
$stmt->execute();
$count = $stmt->fetchColumn();


if($count > 50) {
	$error = 1;
	$error_text = "The entry limit has been reached"; }



$stmt6 = $con->prepare('SELECT COUNT(email) AS EmailCount FROM register2 WHERE email = :email');
$stmt6->execute(array('email' => $_POST['email']));
$result = $stmt6->fetch(PDO::FETCH_ASSOC);

if ($result['EmailCount'] != 0) {
	$error = 1;
	$error_text = "Email already exists!"; }
  
$date = new DateTime($birth);
$result = $date->format('Y-m-d');
 if ($cnp[1]!=$result[2] || $cnp[2]!=$result[3] || $cnp[3]!=$result[5] || $cnp[4]!=$result[6] || $cnp[5]!=$result[8] || $cnp[6]!=$result[9]) {
	$error = 1;
	$error_text = "CNP doesn't match with the birthdate"; }

if($error == 0)
	{ 


$stmt2 = $con -> prepare("INSERT INTO register2(firstname,lastname,phone,email,cnp,facebook,birth,department,question,facultate, sex) VALUES(:firstname,:lastname,:phone,:email,:cnp,:facebook,:birth,:department,:question, :facultate, :sex)");

$stmt2 -> bindParam(':firstname',$firstname);
$stmt2 -> bindParam(':lastname',$lastname);
$stmt2 -> bindParam(':phone',$phone);
$stmt2 -> bindParam(':email',$email);
$stmt2 -> bindParam(':cnp',$cnp);
$stmt2 -> bindParam(':facebook',$facebook);
$stmt2 -> bindParam(':birth',$birth);
$stmt2 -> bindParam(':department',$department);
$stmt2 -> bindParam(':question',$question);
$stmt2 -> bindParam(':facultate',$facultate);
$stmt2 -> bindParam(':sex',$sex);

    if(!$stmt2->execute()) {
    	$errors['connection'] = "Database Error";
                            }


    else {
      echo "Succes";
          }
                 }

else { 
	echo $error_text;
	return;
      }

 ?>